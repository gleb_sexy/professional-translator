# 👅 License Decision

# What license should we choose for the project?
## What do we want to achieve with this project?

We want to make open source available for more people, especially grow open source culture outside software. Give designers, marketers, writers, artists, entrepreneurs and others the ability to contribute to the open source projects and make them better just like coders would develop and make the software better.

## Should everyone be able to modify and distribute the derivatives of this project?

Certainly. This will help to raise the awareness and develop the movement.

## Should everyone be able to distribute the derivatives of this project **without** making the source open? (e.g. under a different license)

IDK HELP.
 


## The project will include not only source code but also design and other decisions. Should they fall under the different license?

MIT, GPL, BSD and some other licenses were designed specifically for the open source **software**. On the other hand there are CC licenses that are not designed and not recommended for software. So should we separate software from the other types of work that is going to be made? I don’t think so. We probably should base our licensing decision on something else first and then just check if it’s ok both for software and other work.

# Some information:

[A comparison table of different licenses.](https://choosealicense.com/appendix/)

[Differences between GPL and MIT license and some arguments on why use one over another.](https://www.quora.com/What-are-the-key-differences-between-the-GNU-General-Public-license-and-the-MIT-License/answer/Gil-Yehuda)
"Each license has its upside and downside.  In the case of GX — the creator of G is assured that any creative contribution made to G will be available to the world — thus continuing his attempt to free the software for all to benefit from.  In contrast, the creator of M will never know how many people are creating MX, MY and MZ products and making money off of their creative additions to M.  But then again, the creator of M might get more people to be willing to participate in the development of M since there is a potential for profitable outcome — whereas in the case of G — making a profit is more difficult (though not at all impossible — after all look at the Linux market).”

There also are some fun licenses:
https://ru.wikipedia.org/wiki/WTFPL
https://github.com/avar/DWTFYWWI/blob/master/DWTFYWWI


