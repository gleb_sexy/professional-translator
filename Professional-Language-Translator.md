# 👅 Professional Language Translator
**Stages**
Described in the [Process](https://paper.dropbox.com/doc/Process--AKZMfDD0dmLAM6HvH03Zo7T9AQ-6HIlAXTSNjbfTIlpapeBp) document

**Below is the initial presentation of the idea which was done on the design sprint.**
You can also find presentation [p](https://photos.app.goo.gl/5FNCpaRkykKYeKsNA)[hotos](https://photos.app.goo.gl/5FNCpaRkykKYeKsNA) here.


- cliche phrases
  - articles
  - memes
  - lokes
  - twitter
- new phrases
  - FB groups
  - user suggestions

**AI**

- understand similar queries
- maybe try to create unique responses
- maybe answer personally (personify, chatbot)
- suggest a reply
- check if written reply is correct

**User**

- designer
- client (human)
- coder, marketer and etc. in the future

**Actions**

- suggest translate
- change/pick a role
- enter text
- see translation
- share

**Can**

- translate
- show reply

**Project**

- not monetizeable
- fun, playful
- usable in real situations
- set and forget
- if community likes it and use it — scalable
- the idea is unique 

**Monetize:**
 $ ads
 $ more translations
 $ more “languages”
 $ V1 — free, V2 — paid
 $ simple 1-time payment

